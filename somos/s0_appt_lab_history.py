from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

@csrf_exempt
def index(request):
    page_dict = dict()
    return render(request, 'somos/history.html', page_dict)