import datetime, json, bson
from collections import OrderedDict
from django.shortcuts import render,  redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from PhmWeb.common import DateUtils, RequestUtils
from PhmWeb.dbmodel.ic_subscribe import IcSubscribe
from PhmWeb.biz.sessionmanager import SessionManager


@csrf_exempt
def index(request):
    action = RequestUtils.get_string(request, 'action')
    if action == 'SaveSubscribe':
        return save_data(request)
    else:
        return init_html_page(request)


def init_html_page(request):
    page_dict = dict()
    page_dict['qid'] = RequestUtils.get_string(request, 'qid')
    return render(request, 'somos/subscribe.html', page_dict)


def save_data(request):
    clinic_id = RequestUtils.get_clinic_id_by_host(request=request)
    res_dict = dict({'Errors': '', 'HasError': False})

    patient_dob = RequestUtils.get_date(request, 'PatientDOB')
    if patient_dob is None:
        res_dict['Errors'] = 'Please fill in a valid date like 01/20/2000'
        res_dict['HasError'] = True
        return JsonResponse(res_dict)

    res_id = IcSubscribe().insert_data(clinic_id, request)
    if res_id < 0:
        res_dict['Errors'] = 'Please fill in a valid date like 01/20/2000'
        res_dict['HasError'] = True
    return JsonResponse(res_dict)
