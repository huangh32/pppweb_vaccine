import random, datetime, json
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from PhmWeb.common import RequestUtils, get_PHM_db
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.biz.sessionmanager import SessionManager, save_http_session, UserSession



def index(request):
    response = HttpResponseRedirect('/p/login/')
    try:
        usession = SessionManager.get_session(request)
        try:
            pass
            # rows = db[LoginTbl].find({'UserName': request.session['username'], 'OrganId': usession.OrganId}).sort([('LoginTime', -1)]).limit(1)
            # db[LoginTbl].update({'_id': rows[0]["_id"]}, {'$set': {'LogoutTime': datetime.datetime.now()}})
        except:
            print(request.session['username'] + ' logout error')
        del request.session['username']
        del request.session['patientid']
        response.delete_cookie('ppp_ussession')
    except KeyError as e:
        print(f"KeyError={e}")
    return response

