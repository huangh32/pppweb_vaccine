import redis, json
import threading
from PhmWeb.utils.dbconfig import get_redis, get_PHM_db
from PhmWeb.common import ObjectEncoder


class RedisCache:
    _instance_lock = threading.Lock()

    def __init__(self):
        self._r = get_redis()

    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(RedisCache, "_instance"):
            with RedisCache._instance_lock:   #keep thread safe , add lock
                if not hasattr(RedisCache, "_instance"):
                    RedisCache._instance = RedisCache(*args, **kwargs)
        return RedisCache._instance

    def get_phm_user(self, user_id):
        jsonstr = self._r.get(f'phm_user_{user_id}')
        if jsonstr:
            return json.loads(jsonstr)
        return None

    def clear_phm_user(self, user_id):
        self._r.delete(f'phm_user_{user_id}')

    def cache_phm_user(self, user):
        user_id = user['UserId']
        self._r.set(f'phm_user_{user_id}', json.dumps(user, cls=ObjectEncoder))

    def get_location_slot_daily_limit(self):
        jsonstr = self._r.get(f'location_slot_daily')
        if jsonstr:
            return json.loads(jsonstr)
        return None

    def get_lock_apt_info(self, lock_key):
        jsonstr = self._r.get(lock_key)
        if jsonstr:
            return json.loads(jsonstr)
        return None

    def cache_lock_apt_info(self, lock_key, apt_dict):
        self._r.set(lock_key, json.dumps(apt_dict, cls=ObjectEncoder))
        self._r.expire(lock_key, 180)
