import os


class EnvironmentVariables:
    def __init__(self):
        pass

    REDIS_HOST = os.environ.get('PHMWEB_VACCINE_REDIS_HOST', '127.0.0.1')  # 192.168.168.106
    ECLINIC_DB67_IP = os.environ.get('ECLINIC_DB67_IP', '127.0.0.1')  # 192.168.168.81 for test, 37 for prod
    ECLINIC_DB67_NAME = os.environ.get('ECLINIC_DB67_NAME', 'ec67')  # ec67 for prod or ec67_2021 for test
    MDLAND_DC_MONGODB = os.environ.get('MDLAND_DC_MONGODB_IP', 'mongodb://127.0.0.1:27017/')
    ENVIRONMENT_TEST = os.environ.get('ENVIRONMENT_TEST', 'default')


if __name__ == "__main__":
    print(EnvironmentVariables.REDIS_HOST)
