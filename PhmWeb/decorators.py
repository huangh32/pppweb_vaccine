import time


def timing_decorator(func):
    def inner(*args, **kwargs):
        start = time.time()
        resp = func(*args, **kwargs)
        end = time.time()
        print("\n==========\n%s - %ss\nargs: %s\nkwargs: %s\n==========\n" % (func.__name__, end - start, args, kwargs))
        return resp
    return inner