import threading, json
import datetime
from PhmWeb.utils.dbconfig import get_PHM_db
from PhmWeb.common import RequestUtils


class DbPhmLogRestAPI:
    _instance_lock = threading.Lock()

    def __init__(self):
        print('')

    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(DbPhmLogRestAPI, "_instance"):
            with DbPhmLogRestAPI._instance_lock:   #keep thread safe , add lock
                if not hasattr(DbPhmLogRestAPI, "_instance"):
                    DbPhmLogRestAPI._instance = DbPhmLogRestAPI(*args, **kwargs)
        return DbPhmLogRestAPI._instance


    #cong phmHedisCache获取统计数据，最新计算的数据
    def add_log(self, restlog, request):
        if not restlog or not request:
            return
        RequestUtils.fill_request_meta_log(request, restlog)
        self.fill_request_info(request, restlog)
        restlog['VisitDate'] = datetime.datetime.now()

        zw = get_PHM_db()
        zw['vac_log_RestAPI'].insert(restlog)
        return None

    def fill_request_info(self, request, restlog):
        # query string, refer url
        get_param = request.GET
        message = ''
        if get_param is not None:
            param_list = []
            for key in get_param:
                param_list.append("{0}={1}".format(key, get_param[key]))
            message += '\nquery string :{0}\n  '.format('&'.join(param_list))

        # post content
        message += '\npost string :{0}\n  '.format(json.dumps(request.POST))
        # refer url
        if request.META and 'HTTP_REFERER' in request.META:
            message += '\nHTTP_REFERER: {0}\n '.format(request.META['HTTP_REFERER'])

        restlog['Message'] = message
        restlog['ParamGet'] = request.GET
        restlog['ParamPost'] = request.POST


    #将数据批量保存到数据库表中
    def save_list_todb(self):
        return None


