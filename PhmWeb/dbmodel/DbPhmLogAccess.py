import datetime
import threading, copy, sys, socket
from PhmWeb.utils.dbconfig import get_PHM_db
from PhmWeb.common import Utils, RequestUtils
#采用单根方式使用
#DbPhmLogAccess.instance()


class DbPhmLogAccess():
    _instance_lock = threading.Lock()

    def __init__(self):
        print('DbPhmLogAccess_init_')
        self.g_log_list = []
        self.host_name = socket.gethostbyname(socket.gethostname())
        ip_port = sys.argv[-1]
        self.host_port = ip_port[ip_port.find(':')+1 : ]

    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(DbPhmLogAccess, "_instance"):
            with DbPhmLogAccess._instance_lock:   #为了保证线程安全在内部加锁
                if not hasattr(DbPhmLogAccess, "_instance"):
                    DbPhmLogAccess._instance = DbPhmLogAccess(*args, **kwargs)
        return DbPhmLogAccess._instance

    #调用方式 DbPhmLogAccess.instance().get_instruction('ABA')
    def add_access_log(self, log):
        if log is not None and 'HostName' not in log:
            log['HostName'] = self.host_name
            log['HostPort'] = self.host_port
        self.g_log_list.append(log)
        self.batch_save_log()

    def batch_save_log(self):
        zw = get_PHM_db()
        try:
          zw['vac_log_Access'].insert(self.g_log_list)
        except Exception as e:
            print('ppp_log_Access insert error')
            print(str(e))

        #cllear 否则会出现id重复
        self.g_log_list.clear()


    def log_key_event(self, log):
        if not log:
            return
        zw = get_PHM_db()
        log['VisitDate'] = datetime.datetime.now()
        zw['vac_log_KyeEvent'].insert(log)

