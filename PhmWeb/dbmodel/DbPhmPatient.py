import datetime
from PhmWeb.utils.dbconfig import get_PHM_db

# this is a backup for any update made to DW_Prod.ACO_Summary
class DbPhmPatient:

    def __init__(self):
        self._dw = None
        self._zw = None
        self._mso_pid = None
        self._patient_uid = None
        self._clinic_id = None

    def set_dw(self, dw):
        self._dw = dw

    def set_zw(self, zw):
        self._zw = zw

    def set_patient_info(self,mso_pid, patient_uid, clinic_id):
        self._mso_pid = mso_pid
        self._patient_uid = patient_uid
        self._clinic_id = clinic_id

    def update_by_qm(self, action, command, measure, mr_result):
        operate_time = datetime.datetime.now()
        if self._zw is None:
            self._zw = get_PHM_db()
        pat = self._zw['phm_Patient'].find_one({'_id': self._mso_pid}, {'QMMeasures':1})
        measures = []
        if pat is None:
            self._zw['phm_Patient'].insert({'_id': self._mso_pid, 'PatientUID': self._patient_uid, 'ClinicID': self._clinic_id, 'CreatedTime': operate_time })
        else:
            measures = pat['QMMeasures'] if 'QMMeasures' in pat else []
        has_measure = False
        for idx, mr in enumerate(measures):
            if measure == mr['Measure']:
                if mr_result is not None:
                    measures.pop(idx)
                    mr_result['DevNote'] = 'measures.pop({0}) {1}'.format(idx, measure)
                    measures.append(mr_result)
                else:
                    mr['ActionName'] = action
                    mr['ActionCommand'] = command
                    mr['OperateDate'] = datetime.datetime.now()
                    mr['DevNote'] = 'has_measure, mr_result None'
                has_measure = True
                break

        if not has_measure:
            if mr_result is not None:
                measures.append(mr_result)
            else:
                mr_item = dict()
                mr_item['Measure'] = measure
                mr_item['ActionName'] = action
                mr_item['ActionCommand'] = command
                mr_item['OperateDate'] = datetime.datetime.now()
                mr_item['DevNote'] = 'not has_measure, mr_result None'
                measures.append(mr_item)

        self._zw['phm_Patient'].update({'_id': self._mso_pid}, {'$set': {'QMMeasures': measures, 'UpdatedTime': operate_time}})

