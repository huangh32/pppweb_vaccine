import logging
import datetime
from PhmWeb.settings import PHM_SETTINGS
import tempfile

# 用字典保存日志级别
import os
import platform
from django.conf import settings

format_dict = {
    logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'),
    logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'),
    logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'),
    logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'),
    logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
}

import os

logger_dict = {}


# 开发一个日志系统， 既要把日志输出到控制台， 还要写入日志文件
class Logger():
    def __init__(self, logger_name, mode='a', encoding='utf-8', delay=False):
        nowdate = datetime.datetime.now().strftime('%Y-%m-%d')
        sysstr = platform.system()

        if PHM_SETTINGS.TEMP_LOG:
            logname = tempfile.gettempdir() + '/PppWeb_{0}.log'.format(nowdate)
        elif (sysstr == "Linux"):
            file_path = "/home/hu/log/"
            if not os.path.exists(file_path):
                os.makedirs(file_path)
            logname = '/home/hu/log/PppWeb.log_{0}'.format(nowdate)
        elif (sysstr == "Darwin"):
            home_path = settings.BASE_DIR
            log_path = "/.log/PppWeb_{0}".format(nowdate)
            logname = home_path + log_path
        else:
            logname = os.getenv("SystemDrive") + '/log/PppWeb_{0}.log'.format(nowdate)

        # logger = "fox"
        '''
           指定保存日志的文件路径，日志级别，以及调用文件
           将日志存入到指定的文件中
        '''

        # 创建一个logger
        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logging.DEBUG)

        # 创建一个handler，用于写入日志文件
        fh = logging.FileHandler(logname)
        fh.setLevel(logging.DEBUG)

        # 再创建一个handler，用于输出到控制台
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        # 定义handler的输出格式
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        # formatter = format_dict[int(loglevel)]
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        if not self.logger.handlers:
            # 给logger添加handler
            self.logger.addHandler(fh)
            self.logger.addHandler(ch)

    def getlog(self):
        return self.logger


def main():
    # logname='log.txt', loglevel=1, logger="fox"
    logger = Logger(logger_name="fox").getlog()
    logger.info('test')
    logger.error('error')


logger = Logger(logger_name="PathReocrd").getlog()

if __name__ == '__main__':
    main()
