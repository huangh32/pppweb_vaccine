from PhmWeb.utils.dbconfig import get_PHM_db
from PhmWeb.common import DictUtils


'''同一个token能用5次'''
def validate_ticket(ticket):
    zw = get_PHM_db()
    sso_login = zw['phm_sys_SsoLogin'].find_one({'Ticket': ticket})
    if sso_login is not None:
        access_count = DictUtils.get_int_value(sso_login, 'AccessCount')
        access_count = access_count + 1
        if access_count < 5:
            zw['phm_sys_SsoLogin'].update({'Ticket': ticket}, {'$set': {'AccessCount': access_count}})
            return True
    return False
